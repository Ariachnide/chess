import React from "react";
import { NavLink } from "react-router-dom";
import sections from "../sections";
import SvgEye from "./icons/SvgEye";

const Header = ({ switchColor, mainColor, secColor, colors, button }) => (
  <header
    style={{
      borderBottom: `2px solid ${secColor}`,
    }}
  >
    <div className="hdLeftBox">
      <div
        onClick={switchColor}
        className="center iconButton"
        style={{ backgroundColor: secColor }}
      >
        <SvgEye color={mainColor} />
      </div>
    </div>
    <div className="hdMidBox">
      <NavLink to="/" className="hdLink" style={{ color: secColor }}>
        Home
      </NavLink>
      {sections.map((section, i) => (
        <NavLink
          to={section.path}
          className="hdLink"
          style={{ color: secColor }}
          key={i}
        >
          {section.name}
        </NavLink>
      ))}
    </div>
    <div className="hdRightBox">
      <div className=""></div>
    </div>
  </header>
);

export default Header;
