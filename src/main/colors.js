/*
  "main" goes for background-color & buttons labels
  "sec" for writings, borders & most buttons
*/

export default [
  {
    main: "#C5EEFF", // very light blue
    sec: "#0090CC", // medium-deep blue
    name: "Day"
  },
  {
    main: "#0050BD", // deep blue
    sec: "#FFFF00", // light yellow
    name: "night"
  },
  {
    main: "#5F5F5F", // deep gray
    sec: "#60E755", // light green mint
    name: "vivid"
  },
  {
    main: "#000000", // black
    sec: "#FF1D1D", // scarlett
    name: "alert"
  }
];
