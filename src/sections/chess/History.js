import React, { useRef, useState, useEffect } from "react";

const History = ({ history, displayHistory, mainColor, secColor }) => {
  const el = useRef();
  const [elScrollHeight, setElScrollHeight] = useState(0);
  const [elScrollTop, setElScrollTop] = useState(0);
  const [elClientHeight, setElClientHeight] = useState(0);

  useEffect(() => {
    if (history.length >= 15) {
      updateFadings();
    }
  }, [history]);

  const updateFadings = () => {
    setElScrollHeight(el.current.scrollHeight);
    setElScrollTop(el.current.scrollTop);
    setElClientHeight(el.current.clientHeight);
  };

  return (
    <div
      className="historyWrapper center"
      style={{
        color: secColor,
        transform: displayHistory ? "scale(1)" : "scale(0)",
        transition: "transform 300ms"
      }}
    >
      <div className="historyHeader" style={{ borderBottom: `2px solid ${secColor}` }}>
        <p>White</p>
        <p>Black</p>
      </div>
      <div className="historyBodyWrapper">
        <div
          className="fadeTop"
          style={{
            backgroundImage: `linear-gradient(${mainColor}, transparent)`,
            opacity: elScrollTop ? "1" : "0",
            transition: "opacity 1s",
          }}
        />
        <div
          className="fadeBottom"
          style={{
            backgroundImage: `linear-gradient(transparent, ${mainColor})`,
            opacity: elScrollHeight
              ? elScrollTop + elClientHeight >= elScrollHeight
                ? "0"
                : "1"
              : "0",
            transition: "opacity 1s",
          }}
        />
        <div className="historyBody" ref={el} onScroll={updateFadings}>
          {history
            .reduce(
              (result, text, i) =>
                i % 2 === 0
                  ? history[i + 1]
                    ? [...result, [text, history[i + 1]]]
                    : [...result, [text, " "]]
                  : result,
              []
            )
            .reverse()
            .map((text, i) => (
              <div key={i} className="historyElement">
                <p>{text[0].split(" ")[1]}</p>
                <p>{text[1].split(" ")[1]}</p>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default History;
