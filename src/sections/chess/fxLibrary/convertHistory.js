const convertHistoryToString = (startX, startY, endX, endY, turn, action) => {
  let move = `${turn}: `;
  if (action === "castling qs") {
    return (move += "O-O-O");
  } else if (action === "castling ks") {
    return (move += "O-O");
  }
  move += `${String.fromCharCode(65 + startX)}${8 - startY}`;
  move += action.includes("take") ? "x" : "-";
  move += `${String.fromCharCode(65 + endX)}${8 - endY}`;
  if (action.includes("upgrade=")) {
    move += action.substring(action.length - 2);
  }
  return move;
};

const convertHistoryToObject = moveStr => {
  const details = moveStr.split(": ");
  const turn = details[0];

  if (details[1].includes("O-O")) {
    return {
      turn,
      startX: 4,
      startY: turn === "white" ? 7 : 0,
      endX: details[1].includes("O-O-O") ? 2 : 6,
      endY: turn === "white" ? 7 : 0
    };
  }

  return {
    turn,
    startX: details[1][0].charCodeAt() - 65,
    startY: -parseInt(details[1][1]) + 8,
    endX: details[1][3].charCodeAt() - 65,
    endY: -parseInt(details[1][4]) + 8
  };
};

export { convertHistoryToString, convertHistoryToObject };
