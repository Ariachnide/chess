export default [
  {
    name: "Soviet",
    black: "crimson",
    white: "silver"
  },
  {
    name: "Steel Blue",
    black: "steelblue",
    white: "lightsteelblue"
  },
  {
    name: "Black & White",
    black: "black",
    white: "white"
  },
  {
    name: "Glass",
    black: "seagreen",
    white: "beige"
  },
  {
    name: "Purple",
    black: "indigo",
    white: "lightblue"
  }
];
