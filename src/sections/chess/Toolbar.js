import React from "react";
import { optionIcons } from "./chessRessources";

const Toolbar = ({
  secColor,
  manageSettingsWindow,
  mainColor,
  displayHistoryWindow,
  historyLength,
  revert
}) => {
  const { HistorySvg, RevertSvg, SettingsSvg } = optionIcons;
  return (
    <div className="toolbar">
      <div
        className="iconButton"
        style={{ backgroundColor: secColor }}
        onClick={manageSettingsWindow}
      >
        <SettingsSvg color={mainColor} />
      </div>
      <div
        className="iconButton"
        style={{ backgroundColor: secColor }}
        onClick={displayHistoryWindow}
      >
        <HistorySvg color={mainColor} />
      </div>
      <div
        className="iconButton"
        style={{
          backgroundColor: secColor,
          opacity: historyLength ? "1" : "0.5",
          cursor: historyLength ? "pointer" : "auto"
        }}
        onClick={revert}
      >
        <RevertSvg color={mainColor} />
      </div>
    </div>
  );
};

export default Toolbar;
